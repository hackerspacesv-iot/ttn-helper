<?php
$lat = (isset($latitude)) ? $latitude : 13.665915; 
$lon = (isset($longitude)) ? $longitude : -88.956942;
?>
   <style>
      #map {
        height: 300px;
      }
    </style>

    <div class="row">
      <?php echo $this->Form->create(); ?>
      <div class="columns large-12">
        <h1>Estimación de Cobertura de Gateway</h1>
        <h2>Ubicación del Gateway</h2>
        <p>Selecciona una ubicación en el mapa o ingresa las coordenadas directamente si las obtienes
        de GPS</p>
      </div>
      <div class="columns large-12" id="map">
      </div>
      <div class="columns large-4">
        <?php echo $this->Form->control('latitude',['type'=>'number','label'=>'Latitud','step'=>'0.000001']) ?>
      </div>
      <div class="columns large-4">
        <?php echo $this->Form->control('longitude',['type'=>'number','label'=>'Longitud','step'=>'0.000001']) ?>
      </div>
      <div class="columns large-4">
        <?php echo $this->Form->control('altitude',['type'=>'number','label'=>'Altitud (m)']); ?>
      </div>
      <div class="columns large-12">
        <p><strong>Importante:</strong> Ingresa la altitud de la antena desde el suelo hasta su ubicación final.
        No ingreses la altura reportada por el GPS.</p>
        <div class="g-recaptcha form-field"></div>
        <?php echo $this->Form->submit('Estimar Cobertura',['class'=>'button']); ?>
      </div>
      <?php echo $this->Form->end(); ?>
<?php if(isset($analysis_id)) { ?>
      <div class="columns large-12">
        <h2>Resultados</h2>
        Permalink (Copia este vínculo para compartir):
        <pre><?php echo "https://ttn.teubi.co/setup/coverage/$analysis_id/$lat/$lon" ?></pre>
        <?php echo $this->Html->link('Descargar KML',"/data/$analysis_id/gateway.kml",['class'=>'button']) ?><br />
        <?php echo $this->Html->link('Descargar configuración para análisis con SPLAT',"/data/$analysis_id/analysis_$analysis_id.zip",['class'=>'button']) ?><br />
      </div>
<?php } ?>
    </div>
    <script>
      var map;
      var marker
      function updateCoords(lat, lon) {
        $('#latitude').val(lat.toFixed(6));
        $('#longitude').val(lon.toFixed(6));
      }
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $lat ?>, lng: <?php echo $lon ?>},
          zoom: 8
        });
        marker = new google.maps.Marker({
          'map': map,
          'draggable': true,
          'title': "GATEWAY",
          'visible': true,
          'position': {lat: <?php echo $lat ?>, lng: <?php echo $lon ?>}
        });
<?php if(isset($analysis_id)) { ?>
        kml_result = new google.maps.KmlLayer({
          'map': map,
          'url': "https://ttn.teubi.co/<?php echo "/data/$analysis_id/gateway.kml" ?>",
          'screenOverlays': true
        });
        map.setZoom(15);
<?php } ?>
        google.maps.event.addListener(map,'click', function(event) {
          marker.setPosition(event.latLng);
          updateCoords(event.latLng.lat(),event.latLng.lng());
        });
        google.maps.event.addListener(marker,'dragend', function(event) {
          marker.setPosition(event.latLng);
          updateCoords(event.latLng.lat(),event.latLng.lng());
        });
        updateCoords(<?php echo $lat ?>,<?php echo $lon ?>);
      }

      var onloadCallback = function() {
        var captchaContainer = document.querySelector('.g-recaptcha');
        grecaptcha.render(captchaContainer, {
          'sitekey' : '<?php echo $recaptcha_sitekey; ?>'
        });
        //document.querySelector('button[type="submit"]').disabled = false;
    };
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $gmaps_apikey ?>&callback=initMap"
    async defer></script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=ES&onload=onloadCallback&render=explicit" async defer></script>
