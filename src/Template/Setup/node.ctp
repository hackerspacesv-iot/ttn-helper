    <div class="row">
      <div class="columns large-12">
        <h1>Generador de Sketch para Nodo</h1>
        <?php echo $this->Form->create(); ?>
        <h2>Información de Acceso</h2>
        <p>Utiliza la información de tu dispositivo en la <a href="https://console.thethingsnetwork.org/applications/">consola 
        de administración de tu cuenta en The Things Network</a></p>
        <?php echo $this->Form->control('dev_addr',['label'=>'Dirección de Dispositivo(Device Address)']) ?>
        <?php echo $this->Form->control('net_key',['label'=>'Llave de Sesión de Red(Network Session Key)']) ?>
        <?php echo $this->Form->control('app_key',['label'=>'Llave de Sesión deApp(App Session Key))']); ?>
        <?php echo $this->Form->submit('Generar Sketch',['class'=>'button']); ?>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>