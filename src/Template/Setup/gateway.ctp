    <div class="row">
      <div class="columns large-12">
        <h1>Generador de Sketch para Gateway</h1>
        <?php echo $this->Form->create(); ?>
        <h2>Descripción del Gateway</h2>
        <?php echo $this->Form->control('desc',['label'=>'Descripción']); ?>
        <?php echo $this->Form->control('email',['label'=>'Email de contacto']) ?>
        <h2>Ubicación del Gateway</h2>
        <p>Puedes utilizar la app de Android 
        <a href="https://play.google.com/store/apps/details?id=com.eclipsim.gpsstatus2&hl=en_US">GPS Status</a>
        para obtener esta información. Recuerda establecer en la 
        <strong>[Configuración]</strong> de <strong>[Unidades]</strong> en 
        <strong>[Formato y datos de posición]</strong> la opción <strong>[DD.DDDDDDº]</strong> 
        y <strong>[metro, km]</strong> en el formato de <strong>[Distancia]</strong>.</p>
        <?php echo $this->Form->control('latitude',['type'=>'number','label'=>'Latitud']) ?>
        <?php echo $this->Form->control('longitude',['type'=>'number','label'=>'Longitud']) ?>
        <?php echo $this->Form->control('altitude',['type'=>'number','label'=>'Altitud']); ?>
        <h2>Información de WiFi</h2>
        <?php echo $this->Form->control('ap_name',['label'=>'Nombre del AP']); ?>
        <?php echo $this->Form->control('ap_key',['type'=>'password','label'=>'Clave de WiFi']); ?>
        <?php echo $this->Form->submit('Generar Sketch',['class'=>'button']); ?>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>