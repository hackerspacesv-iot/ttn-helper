<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use App\Controller\AppController;

class SetupController extends AppController
{
  public function index()
  {
  }

  public function gateway()
  {
  }

  public function node()
  {
  }

  public function coverage($analysis_id=null,$lat=null,$lon=null)
  {
    $request_data = $this->request->getData();
    if($request_data) {
      // Check Re-Captcha
      $recaptcha = new \ReCaptcha\ReCaptcha(Configure::read('APIKeys.RecaptchaPrivKey'));
      $resp = $recaptcha->setExpectedHostname('ttn.teubi.co')
                  ->verify($request_data['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
      if (!$resp->isSuccess()) {
        $this->Flash->error('¡Bienvenidos sean nuestros amos robots!');
        return $this->redirect(['action' => 'coverage']);
      }
      if(!(
        is_numeric($request_data['latitude']) &&
        ($request_data['latitude']>=-90.0 && $request_data['latitude']<=90.0 ) &&
        is_numeric($request_data['longitude']) &&
        ($request_data['longitude']>=-180.0 && $request_data['longitude']<=180.0 ) &&
        is_numeric($request_data['altitude']) &&
        ($request_data['altitude']>=0.0 && $request_data['altitude']<=8000.0 )
        )) {
        $this->Flash->error('Parámetros de análisis inválidos.');
        return $this->redirect(['action' => 'coverage']);
      }

      $analysis_id = date('mdyHis');
      $data_dir = ROOT.'/webroot/data';
      $out_dir = ROOT.'/webroot/data/'.$analysis_id;
      if(!file_exists($out_dir)) {
        mkdir($out_dir);
      }
      if(!file_exists("$out_dir/gateway.az")) {
        symlink("$data_dir/gateway.az", "$out_dir/gateway.az");
      }
      if(!file_exists("$out_dir/gateway.lrp")) {
        symlink("$data_dir/gateway.lrp", "$out_dir/gateway.lrp");
      }
      if(!file_exists("$out_dir/gateway.scf")) {
        symlink("$data_dir/gateway.scf", "$out_dir/gateway.scf");
      }
      if(!file_exists("$out_dir/gateway.qth")) {
        $gateway_qth = fopen("$out_dir/gateway.qth","w");
        $qth_txt = "GATEWAY\r\n".$request_data['latitude'].
          "\r\n".($request_data['longitude']*-1)."\r\n".
          $request_data['altitude']*3.28084."\r\n";
        fwrite($gateway_qth, $qth_txt);
        fclose($gateway_qth);
      }
      shell_exec('cd '.$out_dir.' && splat -olditm -t gateway.qth -o gateway.ppm -d ../ -L 5 -R 5 -db 58 -kml -ngs');
      shell_exec('cd '.$out_dir." && convert gateway.ppm -fuzz 0% -alpha on -channel rgba -fill 'rgba(0,0,0,0.0)' -opaque '#FFFFFFFF' png32:gateway.png");
      shell_exec('cd '.$out_dir." && convert gateway_ck.ppm png32:gatewak_ck.png");
      // TODO: Fix server name
      shell_exec('cd '.$out_dir." && sed -i 's/gateway.ppm/https:\\/\\/ttn.teubi.co\\/data\\/$analysis_id\\/gateway.png/g' gateway.kml");
      shell_exec('cd '.$out_dir." && sed -i 's/gateway_ck.ppm/https:\\/\\/ttn.teubi.co\\/data\\/$analysis_id\\/gateway_ck.png/g' gateway.kml");
      shell_exec('cd '.$out_dir." && sed -i '9 a <color>78ffffff<\\/color>' gateway.kml");
      shell_exec('cd '.$out_dir." && zip -9 analysis_$analysis_id.zip *");
      $this->Flash->set('Análisis completado.');
      // TODO: Fix longitude correction (we assume positive coordinates referenced from W)
      return $this->redirect(['action' => 'coverage', $analysis_id,$request_data['latitude'],$request_data['longitude']]);
    }

    // Set Google MAPS API Key
    $this->set('gmaps_apikey',Configure::read('APIKeys.GoogleMapsAPI'));
    $this->set('recaptcha_sitekey',Configure::read('APIKeys.RecaptchaSiteKey'));
    if($analysis_id!=null) {
      // Validate data and exit if fail
      if(!(
        is_numeric($lat) &&
        ($lat>=-90.0 && $lat<=90.0 ) &&
        is_numeric($lon) &&
        ($lon>=-180.0 && $lon<=180.0 )
        )) {
        $this->Flash->error('Parámetros de análisis inválidos.');
        return $this->redirect(['action' => 'coverage']);
      }
      // Set analysis_id to show map
      $this->set('analysis_id',$analysis_id);
      $this->set('latitude',$lat);
      $this->set('longitude',$lon);

    }
  }
}
